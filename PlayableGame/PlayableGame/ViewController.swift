//
//  ViewController.swift
//  PlayableGame
//
//  Created by タケル on 2020/6/9.
//  Copyright © 2020 krxx. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController, WKUIDelegate {
    
    var webView: WKWebView!

    override func loadView() {
           let webConfiguration = WKWebViewConfiguration()
           webView = WKWebView (frame: .zero, configuration: webConfiguration)
           webView.uiDelegate = self
           view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let controller = UIAlertController(title: "輸入網址", message: "輸入網址及：port", preferredStyle: .alert)
        
        controller.addTextField { (textField) in
           textField.placeholder = "ip位址"
            textField.keyboardType = UIKeyboardType.numbersAndPunctuation
        }
        let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
            let ipAddress:String = (controller.textFields?[0].text)!
            
//            let myURL = URL (string: "http://192.168.1.115:7457")
            let myURL = URL (string: "http://\(ipAddress)")

            let myRequest = URLRequest (url: myURL!)
            self.webView.load(myRequest)
           print(ipAddress)
        }
        controller.addAction(okAction)
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        controller.addAction(cancelAction)
        present(controller, animated: true, completion: nil)
    }

   

}

